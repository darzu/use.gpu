declare module "@use-gpu/wgsl/plot/grid-auto.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const getGridAutoPosition: ParsedBundle;
  export default __module;
}
