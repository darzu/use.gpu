declare module "@use-gpu/wgsl/mask/textured.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const getTextureColor: ParsedBundle;
  export default __module;
}
