# @use-gpu/glyph

```sh
npm install --save @use-gpu/glyph
```

```sh
yarn add @use-gpu/glyph
```

**Docs**: https://usegpu.live/docs/reference-library-@use-gpu-glyph

# Use.GPU - SDF + Font glyph renderer

- rust/wasm `ab_glyph` wrapper
- subpixel distance transform in TS

## Colofon

Made by [Steven Wittens](https://acko.net). Part of `@use-gpu`.

